public class FormatException extends Exception {

    public FormatException(String mes) {

        super("Error en la entrada de la unidad a operar: " + mes);
    }

    public FormatException(String donde, String mes) {

        super("ERROR en " + donde + ". " + mes);
    }
}