import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;

public class Conversor {

    // Conviene usar un LinkedHashMap para mantener el orden en que son introducidas
    // las unidades y facilitar así la generación de la tabla descriptora de
    // unidades
    private LinkedHashMap<String, Unidad> mapaUnidades;

    public Conversor() throws IOException {

        mapaUnidades = new LinkedHashMap<String, Unidad>();

        try (BufferedReader br = new BufferedReader(new FileReader("unidades.txt"))) {

            /*Lee el archivo contenedor de todas las unidades (y sus atributos) con las que puede operar el conversor.
                Se supone el correcto formato del archivo:
                - Agrupando todas las unidades después de una linea indicadora de la magnitud a la que pertenecen, precedida de '!'
                - Listando las unidades una por linea, y respetando el patrón: código(abreviación)#nombreCompleto#relacionUnidadConversion
            */
            String linea;
            String magnitud = null;
            while ((linea = br.readLine()) != null) {

            if (linea.startsWith("!")) {
                magnitud = linea.substring(1);
            }
            else {
                String[] paramsUnidad = linea.split("#");

                mapaUnidades.put(paramsUnidad[0], new Unidad(paramsUnidad[1], Double.parseDouble(paramsUnidad[2]), magnitud));
                }
            }
        }
    }

    public double convertir(String unidadInicial, String unidadFinal, double valor) throws FormatException, IncompatibleMagnitudesException {

        short formInic = 0;
        short formFinal = 0;
        boolean inicValid = true;
        boolean finalValid = true;
        String mes = "";
        //Verifica, por separado, que cada unidad se corresponda con alguna de las que soporta el conversor
        try {
            formInic = compruebaFormato(unidadInicial);
        } catch (FormatException e) {
            inicValid = false;
            mes += e.getMessage();
        }

        try {
            formFinal = compruebaFormato(unidadFinal);
        } catch (FormatException e) {
            finalValid = false;
            
            if (!(mes.equals(e.getMessage()))) {
                mes += (" / " + e.getMessage());
            }
        }

        if (!(inicValid && finalValid)) {
            throw new FormatException((!inicValid ? "unidadInicial / " : "") + (!finalValid ? "unidadFinal" : ""), mes);
        }

        //Comprueba si se está intentando convertir una unidad simple en una compuesta (o viceversa)
        if (formInic == formFinal) {

            //Si las unidades son simples aplica el factor de conversión con la fórmula: valorInic * equivalenciaInic / equivalenciaFinal (siendo 'equivalencia' la proporción guardada con la unidad arbitraria intermedia definida en el archivo unidades.txt)
            if (formInic == 1) {
                if (mapaUnidades.get(unidadInicial).getMagnitud().equals(mapaUnidades.get(unidadFinal).getMagnitud())) {
                    return valor * mapaUnidades.get(unidadInicial).getEquivalencia() / mapaUnidades.get(unidadFinal).getEquivalencia();
                }
                else {
                    throw new IncompatibleMagnitudesException();
                }
            }
            //Si las unidades son compuestas separa sus partes y comprueba las magnitudes de la unidad inicial y la final se correspondan, luego realiza la conversión
            else {
                String[] partesInic = unidadInicial.split("/");
                String[] partesFinal = unidadFinal.split("/");

                //Si tanto las magnitudes del numerador como del denominador de ambas unidades coinciden entre si...
                if (mapaUnidades.get(partesInic[0]).getMagnitud().equals(mapaUnidades.get(partesFinal[0]).getMagnitud())
                    &&
                    mapaUnidades.get(partesInic[1]).getMagnitud().equals(mapaUnidades.get(partesFinal[1]).getMagnitud())) {
                    
                        return valor * mapaUnidades.get(partesInic[0]).getEquivalencia() * mapaUnidades.get(partesInic[1]).getEquivalencia() / (mapaUnidades.get(partesFinal[0]).getEquivalencia() * mapaUnidades.get(partesFinal[1]).getEquivalencia());
                }
                else {
                    throw new IncompatibleMagnitudesException();
                }
            }
        }
        else {
            throw new IncompatibleMagnitudesException();
        }
    }

    /**
     * Función validadora del formato de las unidades especificadas por el usuario. Se asegura de que el programa podrá trabajar con los datos dados.
     * 
     * @param unidad El string pasado como parámetro al método .convertir representando la unidad inicial o final
     * @return Exception -> si el formato es incorrecto. Puede darse si: la fracción (que representa las unidades, p.e.: m/s) es compuesta (ComplexMagnitudeException), la fracción es simple pero alguna de las partes no existe en el archivo unidades.txt o si @unidad no representa una fracción y la unidad no está registrada en el citado archivo (InexistentKeyException).
     *         1 -> si la unidad no es fraccionaria y @unidad se corresponde con alguna unidad registrada.
     *         2 -> si @unidad representa una fracción simple y ambas partes se encuentran registradas como unidades
     */
    private short compruebaFormato(String unidad) throws FormatException {

        if (unidad.contains("/")) {

            String[] partes = unidad.split("/");
            if (partes.length > 2) {
                throw new ComplexMagnitudeException();
            }
            else {
                if (mapaUnidades.containsKey(partes[0]) && mapaUnidades.containsKey(partes[1])) {
                    return 2;
                }
                else {
                    throw new InexistentKeyException("Alguna de las partes de la fracción no se corresponde con ningún código de unidad registrado.");
                }
            }
        }
        else if (mapaUnidades.containsKey(unidad)) {
            return 1;
        }
        else {
            throw new InexistentKeyException("La entrada no se corresponde con ningún código de unidad registrado.");
        }
    }

    /**
     * @return un único String con una tabla formateada conteniendo una fila para cada unidad (agrupadas por magnitud), con una columna izquierda para el nombre de la unidad y una columna derecha para el código de la unidad con el que trabajará el conversor.
     */
    public String generarTablaUnidades() {

        String lineaDiv = "+---------------------------------------+";
        String cabecera = lineaDiv + "\n|               CONVERSOR               |\n";

        String magnitud = null;
        for (String name : mapaUnidades.keySet()) {

            Unidad u = mapaUnidades.get(name);

            if (!u.getMagnitud().equals(magnitud)) {
                magnitud = u.getMagnitud();

                cabecera += (lineaDiv + "\n");
                cabecera += ("| " + magnitud + "\t\t\t\t|\n");
            }

            cabecera += ("|    " + u.getNombre() + "\t\t\t- " + name + "\t|\n");
        }

        return cabecera + lineaDiv;
    }
}