public class InexistentKeyException extends FormatException {

    public InexistentKeyException(String mens) {
       
        super(mens);
    }
}