public class Unidad {

    private String nombre;
    private double equivalencia;
    private String magnitud;

    /**
     * 
     * @param nombre El nombre de la unidad. p.e.: metro, kilometro, segundo, etc.
     * @param equivalencia La relación de la unidad con otra arbitraria y proporcional a todas las demás registradas para calcular las conversiones
     * @param magnitud El orden de la unidad, aquello que mide de la realidad. p.e.: tiempo, volumen, etc.
     */
    public Unidad (String nombre, double equivalencia, String magnitud) {

        this.nombre = nombre;
        this.equivalencia = equivalencia;
        this.magnitud = magnitud;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getEquivalencia() {
        return equivalencia;
    }

    public void setEquivalencia(double equivalencia) {
        this.equivalencia = equivalencia;
    }

    public String getMagnitud() {
        return magnitud;
    }

    public void setMagnitud(String magnitud) {
        this.magnitud = magnitud;
    }
}