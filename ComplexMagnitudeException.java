public class ComplexMagnitudeException extends FormatException {

    public ComplexMagnitudeException() {

        super("El programa no soporta la magnitud compleja (combinación aritmética de magnitudes simples) introducida.");
    }
}