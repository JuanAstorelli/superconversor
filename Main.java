import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        
        Conversor conv = null;
        try {
            conv = new Conversor();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        
        Scanner scan = new Scanner(System.in);
        String unInic, unFinal; 
        double val;

        System.out.println("--BIENVENIDO AL SUPERCONVERSOR--");

        System.out.println("\nEstas son las unidades con las que puedes trabajar.");
        System.out.println(conv.generarTablaUnidades());

        System.out.println("Introduce el valor que quieres convertir: ");
        val = scan.nextDouble();
        scan.nextLine();

        System.out.println("¿Cuál es la unidad de ese valor? (Introduce el código de la columna derecha de la tabla o cualquier combinación de ellos con el formato 'x'/'y') ");
        unInic = scan.nextLine();

        System.out.println("¿A qué unidad quieres convertirlo? ");
        unFinal = scan.nextLine();

        try {
            System.out.println(val + " " + unInic + " = " + conv.convertir(unInic, unFinal, val) + " " + unFinal);
        } catch (FormatException | IncompatibleMagnitudesException e) {
            System.out.println(e.getMessage());
        }

        scan.close();
    }
}