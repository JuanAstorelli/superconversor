public class IncompatibleMagnitudesException extends Exception {

    public IncompatibleMagnitudesException() {
        super("ERROR. No se puede realizar la conversión por incompatibilidad de las magnitudes de las unidades especificadas.");
    }
}